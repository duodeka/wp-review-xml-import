<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://www.linkedin.com/in/hernan-martin-castillo-b6253670/
 * @since             1.0.0
 * @package           Wp_Rxi
 *
 * @wordpress-plugin
 * Plugin Name:       WP Reviews XML Import
 * Plugin URI:        https://github.com/GreenLeewayDesignCastle/WP-Reviews-XML-Import
 * Description:       This plugin imports xml reviews and displays it on specific page.
 * Version:           1.0.0
 * Author:            Clever Code
 * Author URI:        https://www.linkedin.com/in/hernan-martin-castillo-b6253670/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wp-rxi
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'WP_RXI_PlUGIN_DIR_PATH', plugin_dir_path( __FILE__ ) );
define( 'WP_RXI_PlUGIN_TEMPLATE_PATH', WP_RXI_PlUGIN_DIR_PATH.'templates');
define( 'WP_RXI_POST_TYPE', 'rxi_review');
/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_NAME_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wp-rxi-activator.php
 */
function activate_wp_rxi() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-rxi-activator.php';
	Wp_Rxi_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wp-rxi-deactivator.php
 */
function deactivate_wp_rxi() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-rxi-deactivator.php';
	Wp_Rxi_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wp_rxi' );
register_deactivation_hook( __FILE__, 'deactivate_wp_rxi' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wp-rxi.php';

/**
 * The core plugin class that is used to define shortcodes,
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wp-rxi-shortcode.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_wp_rxi() {

	$plugin = new Wp_Rxi();
	$wp_rxi_shortcode = new Wp_Rxi_Shortcode($plugin);
	$plugin->run();

}
run_wp_rxi();

