<?php

/**
 * Register all shortcodes for the plugin
 *
 * @link       https://www.linkedin.com/in/hernan-martin-castillo-b6253670/
 * @since      1.0.0
 *
 * @package    Wp_Rxi
 * @subpackage Wp_Rxi/includes
 */

/**
 * Register all shortcodes for the plugin.
 *
 * Maintain a list of all shortcodes that are registered throughout
 * the plugin, and register them with the WordPress API.
 *
 * @package    Wp_Rxi
 * @subpackage Wp_Rxi/includes
 * @author     Hernan Martin Castillo <hermartin@gmail.com>
 */
class Wp_Rxi_Shortcode {

    private $main_plugin;

    /**
     * @param Wp_Rxi $main_plugin_class
     */

    public function __construct($main_plugin_class) {
        $this->main_plugin = $main_plugin_class;
        add_shortcode('rxi_show_reviews', array( $this, 'wp_rxi_show_reviews') );

    }

    public function wp_rxi_show_reviews()
    {
        return $this->get_template_content('reviews-content');
    }
    /**
     * Puts output in buffering and return contents
     *
     * @param string $template_name The template filename to be used
     * @param array Variables passed to the template file
     * @return string Template contents
     */

    private function get_template_content($template_name = 'default', $params = array())
    {
        extract($params); // Array is now available as variable (array key = variable)
        ob_start();
        include(WP_RXI_PlUGIN_TEMPLATE_PATH.'/'.$template_name.'.php');
        $return = ob_get_contents();
        ob_end_clean();
        return $return;

    }
}
