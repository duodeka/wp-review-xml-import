<?php

/**
 * Fired during plugin activation
 *
 * @link       https://www.linkedin.com/in/hernan-martin-castillo-b6253670/
 * @since      1.0.0
 *
 * @package    Wp_Rxi
 * @subpackage Wp_Rxi/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wp_Rxi
 * @subpackage Wp_Rxi/includes
 * @author     Hernan Martin Castillo <hermartin@gmail.com>
 */
class Wp_Rxi_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
	    self::create_plugin_database_table();

	}

    private static function create_plugin_database_table() {
        global $wpdb, $rxi_db_version;

        $rxi_db_version = '1.0';
        $table_name = $wpdb->prefix . "rxi_reviews";
        $charset_collate = $wpdb->get_charset_collate();

        if($wpdb->get_var( "show tables like '$table_name'" ) != $table_name) {
            $sql = "CREATE TABLE $table_name (
                id mediumint(9) NOT NULL AUTO_INCREMENT,
                name VARCHAR(255) NOT NULL, 
                place VARCHAR(255) NOT NULL, 
                date datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
                total_scores INT DEFAULT 0,
                recommendation VARCHAR(255) NOT NULL, 
                positive text NULL,
                negative text NULL,
                comment text NULL,
                check_id int NOT NULL,
                PRIMARY KEY  (id)
                ) $charset_collate;";

            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql);
            add_option("rxi_db_version",  $rxi_db_version);
        }
    }
}
