<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

$kiyohPlugin = new \KiyOh_Klantenvertellen\KiyOh_Klantenvertellen_Plugin();
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$paginationAmount = apply_filters('wp_rxi_get_plugin_option', 'pagination-amount');
$args = array(
        'posts_per_page' => $paginationAmount,
        'paged' => $paged,
        'post_type' => WP_RXI_POST_TYPE,
        'orderby' => 'post_date',
        'order' => 'DESC'
);
$custom_query = new WP_Query($args);
?>
<div class="container-fluid">
    <div class="row">
        <?php if ($custom_query->have_posts()): ?>
            <div class="col-md-12" id="number-of-reviews">
                <h3 style="text-align: right;">
                    <?php echo apply_filters('wp_rxi_count_reviews', []); ?> Beoordelingen
                </h3>
            </div>
        <?php
            while($custom_query->have_posts()) : $custom_query->the_post();
            $reviewId = (int) get_the_content();
            $review = apply_filters('wp_rxi_get_review_by_id', $reviewId);
            if (!$review) continue;
            $date = new DateTime($review['date']);
        ?>
            <div class="col-md-6">
                <div class="review-panel">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="name">
                                <strong><?php echo ($review['name']) ? $review['name'] : 'Anoniem'; ?></strong>
                                <span>
                                    <?php echo ($review['place']) ? $review['place'] : ''; ?>
                                </span>
                            </div>
                            <div class="score">
                                <?php echo ($review['total_scores']) ? $review['total_scores'] : ''; ?>
                            </div>
                            <div class="recommendation">
                                Ja, ik beveel dit aan
                            </div>

                        </div>
                        <div class="col-sm-8">
                            <span class="date"><?php echo ($review['date']) ? $date->format('j-m-Y') : ''; ?></span>
                            <div class="text">
                                <?php if($review['total_scores']){ ?>
                                    <div class="kk-reviews on-image">
                                        <div class="kk-rating">
                                            <div class="kk-rating-stars">
                                                <?php $stars = $kiyohPlugin::render_star_rating($review['total_scores']); ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>

                                <?php
                                if($review['positive']){
                                    ?><strong>Ervaring:</strong><br><?php
                                    echo $review['positive'];
                                }else{
                                    echo 'Ik beveel Klikgordijn aan.';
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
            <div class="col-md-12 ">
                <?php echo apply_filters('wp_rxi_pagination_reviews', $custom_query->max_num_pages); ?>
            </div>
        <?php else: ?>
            <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
        <?php endif; ?>
    </div>
</div>
