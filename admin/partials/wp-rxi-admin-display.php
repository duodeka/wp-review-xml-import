<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://www.linkedin.com/in/hernan-martin-castillo-b6253670/
 * @since      1.0.0
 *
 * @package    Wp_Rxi
 * @subpackage Wp_Rxi/admin/partials
 */
$options = get_option($this->plugin_name);
$paginationAmount = $options['pagination-amount'];
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<div class="wrap">
    <h2><?php echo esc_html(get_admin_page_title()); ?></h2>
    <form method="post" name="review_options" action="options.php">
        <?php
            settings_fields($this->plugin_name);
            do_settings_sections($this->plugin_name);
        ?>
            <label for="<?php echo $this->plugin_name; ?>-pagination-amount">Beoordelingen pagina tonen maximaal</label>
                <input name="<?php echo $this->plugin_name; ?>[pagination-amount]"
                       type="number"
                       step="1"
                       min="1"
                       max="<?php echo apply_filters('wp_rxi_count_reviews', []); ?>"
                       id="<?php echo $this->plugin_name; ?>-pagination-amount"
                       value="<?php echo $paginationAmount ?>"
                       class="small-text">

        <?php submit_button('Save all changes', 'primary','submit', TRUE); ?>
    </form>
</div>
