<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://www.linkedin.com/in/hernan-martin-castillo-b6253670/
 * @since      1.0.0
 *
 * @package    Wp_Rxi
 * @subpackage Wp_Rxi/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Wp_Rxi
 * @subpackage Wp_Rxi/public
 * @author     Hernan Martin Castillo <hermartin@gmail.com>
 */

class Wp_Rxi_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;
    private $kiyohPlugin;
	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */

	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
        $this->wp_rxi_options = get_option($this->plugin_name);
        $this->kiyohPlugin = new \KiyOh_Klantenvertellen\KiyOh_Klantenvertellen_Plugin();
    }

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wp_Rxi_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wp_Rxi_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wp-rxi-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Wp_Rxi_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Wp_Rxi_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wp-rxi-public.js', array( 'jquery' ), $this->version, false );

	}

    public function wp_rxi_save_reviews(){
        global $wpdb;
        $url = $this->get_request_url();
        if ($url){
            $totalReviews = (int) $this->kiyohPlugin->get_kiyoh_data()->company->total_reviews;
            $urlAllReviews = add_query_arg('reviewcount', $totalReviews, $url);
            $data = $this->do_request($urlAllReviews);
            $reviews = $data->review_list->review;
            if (!empty($reviews)) {
                foreach ($reviews as $review){
                    $exist = $wpdb->get_var(sprintf( "SELECT check_id FROM %s where check_id = %d ;", $wpdb->prefix . 'rxi_reviews', $review->id));
                    if(!$exist){
                        $wpdb->insert(
                            $wpdb->prefix . 'rxi_reviews',
                            array(
                                'name'         => $review->customer->name,
                                'place'        => $review->customer->place,
                                'date'         => $review->customer->date,
                                'total_scores' => (int) $review->total_score,
                                'positive'     => $review->positive,
                                'negative'     => $review->negative,
                                'comment'      => "",
                                'check_id'     => $review->id
                            )
                        );

                        $my_post = array(
                            'title' => 'Review',
                            'post_status' => 'publish',
                            'post_content' => (int) $review->id,
                            'post_type' => WP_RXI_POST_TYPE,
                            'post_date' => (string) $review->customer->date,
                        );

                        wp_insert_post($my_post, true);
                    }
                }
            }
        }

    }

    public function wp_rxi_get_review_by_id($id)
    {
        global $wpdb;
        $review = $wpdb->get_results(sprintf( "SELECT name, `place`, date, `total_scores`, `positive`, total_scores FROM %s WHERE check_id = %d;", $wpdb->prefix . 'rxi_reviews', $id), ARRAY_A);

        return (!empty($review)) ? $review[0] : null;
    }

    public function wp_rxi_count_reviews()
    {
        global $wpdb;
        $reviewsCount = $wpdb->get_var(sprintf( "SELECT count(*) from %s;", $wpdb->prefix . 'rxi_reviews'));

        return $reviewsCount;
    }

    public function wp_rxi_get_plugin_option($option)
    {
        $pluginOption = (array_key_exists($option, $this->wp_rxi_options)) ? $this->wp_rxi_options[$option] : 0;

        return $pluginOption;
    }

    public function wp_rxi_pagination_reviews($pages = '', $range = 4)
    {
        global $paged;
        global $wp_query;
        $showitems = ($range * 2)+1;

        if($pages == '') {
            $pages = $wp_query->max_num_pages;
            if (!$pages) {
                $pages = 1;
            }
        }

        if(1 != $pages) {
            echo "<div class=\"pagination\"><span>Pagina " . $paged . " van " . $pages . "</span>";
            if ($paged > 2 && $paged > $range + 1 && $showitems < $pages) echo "<a href='" . get_pagenum_link(1) . "'>&laquo; First</a>";
            if ($paged > 1 && $showitems < $pages) echo "<a href='" . get_pagenum_link($paged - 1) . "'>&lsaquo; Previous</a>";

            for ($i = 1; $i <= $pages; $i++) {
                if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems)) {
                    echo ($paged == $i) ? "<span class=\"current\">" . $i . "</span>" : "<a href='" . get_pagenum_link($i) . "' class=\"inactive\">" . $i . "</a>";
                }
            }

            if ($paged < $pages && $showitems < $pages) echo "<a href=\"" . get_pagenum_link($paged + 1) . "\">Next &rsaquo;</a>";
            if ($paged < $pages - 1 && $paged + $range - 1 < $pages && $showitems < $pages) echo "<a href='" . get_pagenum_link($pages) . "'>Last &raquo;</a>";
        echo "</div>\n";
        }
    }

    private function do_request($url){

        $response = wp_remote_get( $url );

        if (is_wp_error($response)) {

            echo $response->get_error_message(); // TODO Make proper error handling function

        } else {

            $body = wp_remote_retrieve_body($response);

            if (!empty($body)) {

                return simplexml_load_string($body);
            }

        }

        return false;

    }

    private function get_request_url(){
        $providerUrlKiyohXml = 'https://www.kiyoh.nl/xml/recent_company_reviews.xml';
        if (isset( $this->kiyohPlugin->plugin_settings['general']['kiyoh_connectorcode'] ))
            $connectorcode = $this->kiyohPlugin->plugin_settings['general']['kiyoh_connectorcode'];

        if (isset($this->kiyohPlugin->plugin_settings['general']['kiyoh_company_id']))
            $company_id = $this->kiyohPlugin->plugin_settings['general']['kiyoh_company_id'];

        if (!empty($connectorcode) && !empty($company_id))  return $providerUrlKiyohXml.'?connectorcode=' . $connectorcode . '&company_id=' . $company_id;

        return false;
    }
}
