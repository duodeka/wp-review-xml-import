<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://www.linkedin.com/in/hernan-martin-castillo-b6253670/
 * @since      1.0.0
 *
 * @package    Wp_Rxi
 * @subpackage Wp_Rxi/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
